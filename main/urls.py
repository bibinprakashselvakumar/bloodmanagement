"""bloodmanagement URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('',views.homepage, name='home'),
    path('home',views.homepage, name='home'),
    path('about',views.aboutpage,name='about'),
    path('login',views.loginpage,name='login'),
    path('gift',views.giftpage,name='gift'),
    path('contact',views.contactpage,name='contact'),
    path('signup',views.signuppage,name='signup'),
    path('register',views.register,name='register'),
    path('search',views.searchpage,name='search'),
    path('find',views.search,name='find'),
    path('update',views.update,name='update'),
    path('lastdonation',views.updatepage,{'option' : 'lastdonation'},name='updatepage'),
    path('preferredgap',views.updatepage,{'option' : 'preferredgap'},name='updatepage'),
    path('place',views.updatepage,{'option' : 'place'},name='updatepage'),
    path('getin',views.login,name='getin'),
    path('getout',views.logout,name='getout'),
    path('donate',views.donate,name='donate'),
    path('donatepage',views.donatepage,name='donatepage'),
    path('bloodbanks',views.bloodbanks,name='bloodbanks'),
]
