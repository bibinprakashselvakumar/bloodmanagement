from .models import Donor,Acceptor,Bloodbanks,Donations
from django.shortcuts import render
import datetime
from . import sendmail

def giftpage(request):
    ds = {}
    try:
        ds = Donations.objects.order_by("-id")[:10]
    except:
        pass
    return render(request,'main/gift.html',{'ds':ds})

def register(request):
    name = request.POST['name']
    email = request.POST['email']
    password = request.POST['password']
    cpassword = request.POST['cpassword']
    birthday = request.POST['birthday']
    birthday = birthday.split('/')
    birthday = datetime.date(int(birthday[2]),int(birthday[1]),int(birthday[0]))
    gender = request.POST['gender']
    blood_group = request.POST['blood_group']
    phone = request.POST['phone']
    place = request.POST['city']
    user_type = request.POST['user_type']
    
    if user_type == 'donor':
        validno = Donor.objects.filter(phone=phone).count()
    else:
        validno = Acceptor.objects.filter(phone=phone).count()
    
    if(validno!=0):
        return render(request,'main/result.html',{'msg1':'Registration Failed','msg2':'This number already has an account','msg3':'Register again','msg4':'lh'})
    
    if(password!=cpassword):
        return render(request,'main/result.html',{'msg1':'Registration Failed','msg2':'Password and Confirm password Should be same.','msg3':'Register again','msg4':'slh'})
    
    if user_type == 'donor':
        today = datetime.date.today()
        
        if((today.year-birthday.year)<=18):
            return render(request,'main/result.html',{'msg1':'Registration Failed','msg2':'Under age','msg3':'Register after 18','msg4':'lh'})
    
    password = str(hash(password))
    
    try:
        if user_type == 'donor':
            d = Donor(name=name,email=email,password=password,birthday=birthday,gender=gender,blood_group=blood_group,phone=phone,place=place)
            d.save()
        else:
            d = Acceptor(name=name,email=email,password=password,birthday=birthday,gender=gender,blood_group=blood_group,phone=phone,place=place)
            d.save()
    except:
        return render(request,'main/result.html',{'msg1':'Registration Failed','msg2':'Account Cannot be created','msg3':'Register again','msg4':'slh'})
    
    return render(request,'main/result.html',{'msg1':'Registration Successful','msg2':'Account created successfully','msg3':'Proceed to login','msg4':'lh'})

def login(request,loggedin=False):
    
    if(loggedin):
        phone = request.COOKIES.get('phone',None)
    else:
        phone = request.POST['phone']
        password = str(hash(request.POST['password']))
        user_type = request.POST['user_type']
    
    try:
        print(phone)
        if user_type == 'donor':
            user_object = Donor.objects.get(phone=phone)
        else:
            user_object = Acceptor.objects.get(phone=phone)
    except:
        return render(request,'main/result.html',{'msg1':'Login Failed','msg2':'Account not found','msg3':'Please Register','msg4':'slh'})
    
    if((not loggedin) and user_object.password!=password):
        return render(request,'main/result.html',{'msg1':'Login Failed','msg2':'Incorrect password','msg3':'Try again','msg4':'lh'})
    
    name = user_object.name
    phone = user_object.phone
    
    if user_type == 'donor':
        response = render(request,'main/dashboard.html',{'name':name})
    else:
        response = render(request,'main/adashboard.html',{'name':name})
        
    response.set_cookie(key='phone',value=phone)
    
    return response

def update(request):
    #options are set in html file
    print(request.POST['option'])
    
    option = request.POST['option']
    phone = request.COOKIES.get('phone')
    try:
        #updates the option in Donor table with key phone
        user = Donor.objects.get(phone=phone)
        
        if(option == 'place'):
            place = request.POST['city']
            user.place = place
            user.save()
            return render(request,'main/result.html',{'msg1':'Successfully updated','msg2':'Place Updated','msg3':'Go to home','msg4':'l'})
        elif(option == 'last_donation'):
            last_donation = request.POST['lastdonation']
            last_donation = last_donation.split('/')
            last_donation = datetime.date(int(last_donation[2]),int(last_donation[1]),int(last_donation[0]))
            user.last_donation = last_donation
            user.save()
            return render(request,'main/result.html',{'msg1':'Successfully updated','msg2':'Last Donation Updated','msg3':'Go to home','msg4':'l'})
        elif(option == 'preferred_gap'):
            preferred_gap = request.POST['preferredgap']
            user.preferred_gap = preferred_gap
            user.save()
            return render(request,'main/result.html',{'msg1':'Successfully updated','msg2':'Preferred gap Updated','msg3':'Go to home','msg4':'l'})
        
    except:
        return render(request,'main/result.html',{'msg1':'Update Failed','msg2':'Unknown error','msg3':'Go to home','msg4':'l'})
    return render(request,'main/result.html',{'msg1':'Update Failed','msg2':'Unknown error','msg3':'Go to home','msg4':'l'})

def search(request):
    data = {}
    data['place'] = request.POST['city']
    data['blood_group'] = request.POST['blood_group']
    data['phone'] = request.POST['phone']
    data['hospital'] = request.POST['hospital']
    
    try:
        donors = Donor.objects.filter(place=data['place'],blood_group=data['blood_group'])
        #donors = Donor.objects.all()
        n = 0
        data['emails'] = []
        for d in donors:
            data['emails'].append(d.email)
            n += 1
        
    except:
        response = render(request,'main/result.html',{'msg1':'Search failed','msg2':'Unknown error','msg3':'Retry','msg4':'fh'})
        response.set_cookie(key='place',value=data['place'])
        return response
    
    if(n>0):
        #sendmail.sendemail(data)
        response = render(request,'main/result.html',{'msg1':'Donors found','msg2':'Mail has been sent to {} donors'.format(n),'msg3':'For Bloodbank details:','msg4':'bfh'})
        response.set_cookie(key='place',value=data['place'])
        return response
    else:
        response = render(request,'main/result.html',{'msg1':'No donor found','msg2':'No donors available at your place','msg3':'For Bloodbank details','msg4':'bfh'})
        response.set_cookie(key='place',value=data['place'])
        return response

def donate(request):
    sphone = request.COOKIES.get('phone','123')
    rphone = request.POST['bdn']
    amount = request.POST['amount']
    
    try:
        sname = Acceptor.objects.get(phone=sphone)
        rname = Donor.objects.get(phone=rphone)
        d = Donations(sname=sname,rname=rname,amount=amount)
        d.save()
    except:
        return render(request,'main/result.html',{'msg1':'Error','msg2':'Receiver not registered','msg3':'Please check the number','msg4':'lo'})
    
    return render(request,'main/result.html',{'msg1':'Donation successful','msg2':'Thanks for supporting','msg3':'Have a good day','msg4':'lo'})

def bloodbanks(request):
    place = request.COOKIES.get('place',None)
    try:
        bb = Bloodbanks.objects.filter(place=place)
        return render(request,'main/bdashboard.html',{'bbs':bb})
    except:
        return render(request,'main/result.html',{'msg1':'Unknown Error','msg2':'Sorry for inconvenience','msg3':'It will be corrected soon','msg4':'fh'})