from django.shortcuts import render
from . import dbmod

# Create your views here.

def searchpage(request):
    return render(request,'main/search.html',{'option':'password'})

def homepage(request):
    return render(request,'main/home.html')

def aboutpage(request):
    return render(request,'main/about.html')

def loginpage(request):
    phone = request.COOKIES.get('phone',None)
    if(phone != None):
        return dbmod.login(request,True)
    return render(request,'main/login.html')

def giftpage(request):
    return dbmod.giftpage(request)

def contactpage(request):
    return render(request,'main/contact.html')

def signuppage(request):
    return render(request,'main/signup.html')

def donatepage(request):
    return render(request,'main/donate.html')

def register(request):
    return dbmod.register(request)

def login(request):
    return dbmod.login(request)

def logout(request):
    response = render(request,'main/login.html')
    response.delete_cookie(key='phone')
    return response

def update(request):
    return dbmod.update(request)

def updatepage(request,option):
    return render(request,'main/update.html',{'option': option })

def search(request):
    return dbmod.search(request)

def donate(request):
    return dbmod.donate(request)

def bloodbanks(request):
    return dbmod.bloodbanks(request)